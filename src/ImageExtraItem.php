<?php

/**
 * @file
 * Contains \Drupal\image_field_extra\ImageExtraItem.
 */

namespace Drupal\image_field_extra;

use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;

class ImageExtraItem extends ImageItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return array(
      'default_image' => array(
        'photo_credit' => '',
        'caption_title' => '',
        'caption_text' => '',
      ),
      'photo_credit_field' => FALSE,
      'photo_credit_field_required' => FALSE,
      'caption_title_field' => FALSE,
      'caption_title_field_required' => FALSE,
      'caption_text_field' => FALSE,
      'caption_text_field_required' => FALSE,
    ) + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    // Get base form from ImageItem.
    $element = parent::fieldSettingsForm($form, $form_state);
    // Get field settings.
    $settings = $this->getSettings();
    // Get the default field settings.
    $settings_default = self::defaultFieldSettings();

    // Add photo_credit option.
    $element['photo_credit_field'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable <em>Photo Credit</em> field'),
      '#default_value' => (!empty($settings['photo_credit_field'])) ? $settings['photo_credit_field'] : $settings_default['photo_credit_field'],
      '#description' => t('Adds an extra text field for photo credit on image fields.'),
      '#weight' => 13,
    );
    // Add photo_credit (required) option.
    $element['photo_credit_field_required'] = array(
      '#type' => 'checkbox',
      '#title' => t('<em>Photo Credit</em> field required'),
      '#default_value' => $settings['photo_credit_field_required'],
      '#description' => '',
      '#weight' => 14,
      '#states' => array(
        'visible' => array(
          ':input[name="settings[photo_credit_field]"]' => array('checked' => TRUE),
        ),
      ),
    );
    // Add caption title option.
    $element['caption_title_field'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable <em>Caption Title</em> field'),
      '#default_value' => (!empty($settings['caption_title_field'])) ? $settings['caption_title_field'] : $settings_default['caption_title_field'],
      '#description' => t('Adds an extra text field for caption title on image fields.'),
      '#weight' => 15,
    );
    // Add caption title (required) option.
    $element['caption_title_field_required'] = array(
      '#type' => 'checkbox',
      '#title' => t('<em>Caption Title</em> field required'),
      '#default_value' => $settings['caption_title_field_required'],
      '#description' => '',
      '#weight' => 16,
      '#states' => array(
        'visible' => array(
          ':input[name="settings[caption_title_field]"]' => array('checked' => TRUE),
        ),
      ),
    );
    // Add caption text option.
    $element['caption_text_field'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable <em>Caption Text</em> field'),
      '#default_value' => (!empty($settings['caption_text_field'])) ? $settings['caption_text_field'] : $settings_default['caption_text_field'],
      '#description' => t('Adds an extra text area for captions on image fields.'),
      '#weight' => 17,
    );
    // Add caption text (required) option.
    $element['caption_text_field_required'] = array(
      '#type' => 'checkbox',
      '#title' => t('<em>Caption Text</em> field required'),
      '#default_value' => $settings['caption_text_field_required'],
      '#description' => '',
      '#weight' => 18,
      '#states' => array(
        'visible' => array(
          ':input[name="settings[caption_text_field]"]' => array('checked' => TRUE),
        ),
      ),
    );

    // Add default photo credit.
    $element['default_image']['photo_credit'] = array(
      '#type' => 'value',
      '#value' => (!empty($settings['default_image']['photo_credit'])) ? $settings['default_image']['photo_credit'] : $settings_default['default_image']['photo_credit'],
    );
    $element['default_image']['caption_title'] = array(
      '#type' => 'value',
      '#value' => (!empty($settings['default_image']['caption_title'])) ? $settings['default_image']['caption_title'] : $settings_default['default_image']['caption_title'],
    );
    $element['default_image']['caption_text'] = array(
      '#type' => 'value',
      '#value' => (!empty($settings['default_image']['caption_text'])) ? $settings['default_image']['caption_text'] : $settings_default['default_image']['caption_text'],
    );

    return $element;
  }
}
